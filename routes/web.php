<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login');
});


Route::get('/register', function () {
    return view('pages.register');
});


Route::get('/list-products', function () {
    return view('pages.register');
});


/*
 * Route::get('/list-products', function () {
    return view('pages.listproducts');
});

*/
//products
Route::get('/list-products', 'ProductManage@index');
Route::get('/products', 'ProductManage@create');
Route::get('/product-json', 'ProductManage@ProductsJson');
Route::get('/supplier-json', 'ProductManage@SupplierJson');

//post
Route::post('/products', 'ProductManage@store')->name('product.submit');


///stocks
Route::get('/add-stocks', 'ManageStock@AddStock');
Route::get('/view-stocks', 'ManageStock@ViewAllStocks');

//post
Route::post('/add-stocks', 'ManageStock@StoreStock')->name('stocks.submit');



///supplier  ManageSupplier

Route::get('/add-supplier', 'ManageSupplier@index');
Route::get('/view-supplier', 'ManageSupplier@ViewAllSupplier');
//post
Route::post('/add-supplier', 'ManageSupplier@StoreSupplier')->name('supplier.submit');