@extends('structure')
@section('body_class', 'fix-header')
@section('content')


    {{--  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     --}}
    <link href="{{asset('css/easy-autocomplete.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/easy-autocomplete.themes.min.css')}}" rel="stylesheet">
    <div id="wrapper">
        @include('components.header');
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('components.menu')

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Add New Products</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Get Support</a>
                        <ol class="breadcrumb">
                            <li><a href="/">Dashboard</a></li>
                            <li><a href="/">Manage Products</a></li>
                            <li class="active">Add New Products</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- main page content -->


                <div class="row">

                    <p class="text-muted m-b-30"> </p>
                    <form class="form-material form-horizontal" role="form"  role="form" method="POST" action="{{ route('stocks.submit') }}">
                        {{csrf_field()}}

                        <div class="col-sm-4">

                            <div class="white-box">
                                <h3 class="box-title m-b-0"><strong>Add New Stock Count</strong></h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                </div>
                                <div class="form-group">
                                    <label><strong>Purchase No</strong></label>

                                    <input type="text" name="purchase_number" required class="form-control form-control-line" placeholder="ABC123" value="">
                                    <span class="help-block">
                                        <small>Add Purchase Number from Supplier Invoice</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>Supplier Invoice Number</strong></label>

                                    <input type="text" name="invoice_number"  class="form-control form-control-line" placeholder=" eg: 200ML Oil" value="">
                                    <span class="help-block">
                                        <small>Add Invoice Number of Supplier</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>Date of Purchase</strong></label>

                                    <input type="date" name="date_of_purchase"  class="form-control form-control-line" placeholder="date" value="">
                                    <span class="help-block">
                                        <small>Date of Purchase</small>
                                    </span>

                                </div>





                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="white-box">
                                <h3 class="box-title m-b-0"><strong>Supplier's Details</strong></h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                </div>

                                 <div class="form-group">


                                    <label><strong>Supplier Name</strong></label>
                                    <div >
                                        <input type="text" name="supplier_name" id="supplier_name" class="form-control form-control-line" placeholder="Start Tying a name" value="">

                                    </div>

                                     <label><strong>Supplier Account ID</strong></label>
                                    <div >
                                        <input type="text" name="supplier_id" id="supplier_id" class="form-control form-control-line" placeholder="Start Tying a name" value="">

                                    </div>
                                    <label><strong>Address</strong></label>
                                    <div >
                                        <input type="text" name="address" id="address" class="form-control form-control-line" placeholder="Start Tying a name" readonly value="">

                                    </div>

                                     <label><strong>GSTIN</strong></label>
                                    <div >
                                        <input type="text" name="gstin" id="gstin" class="form-control form-control-line" placeholder="Start Tying a name" readonly value="">

                                    </div>


                                     <label><strong>State</strong></label>
                                    <div >
                                        <input type="text" name="state" id="state" class="form-control form-control-line" readonly placeholder="Start Tying a name" value="">

                                    </div>


                                </div>

                            </div>

                        </div>



                        <div class="col-sm-2"></div>

                        {{--next--}}

                        <div class="col-md-4">
                            <div class="white-box">
                                <h3 class="box-title m-b-0"><strong>Product Details</strong></h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                </div>


                                <label><strong>Product Name</strong></label>
                                <div >
                                    <input type="text" name="product_name" id="product_name" class="form-control form-control-line" placeholder="Start Tying a name" value="">

                                </div>


                                <label><strong>Product ID</strong></label>
                                <div >
                                    <input type="text" name="product_id" id="product_id" class="form-control form-control-line" placeholder="Start Tying a name" value="">

                                </div>
                                <!!-- quantity details-->
                                <div class="col-md-8">
                                <label><strong>Quantity</strong></label>
                                <div >
                                    <input type="text" name="quantity"  class="form-control form-control-line" placeholder="Quanty in Stock" value="">

                                </div>
                                </div>

                                <div class="col-md-4">
                                <label><strong>Unit</strong></label>
                                <div >
                                    <input type="text" name="unit" id="unit"  class="form-control form-control-line" placeholder="Quanty in Stock" value="">

                                </div>
                                </div>
                                <!!-- quantity details-->
                                <!!-- gst details-->

                                <div class="col-md-4">
                                <label><strong>IGST</strong></label>
                                <div >
                                    <input type="text" name="igst" id="igst" class="form-control form-control-line" placeholder="IGST" value="">

                                </div>

                                </div>


                                <div class="col-md-4">
                                <label><strong>CGST</strong></label>
                                <div >
                                    <input type="text" name="cgst" id="cgst" class="form-control form-control-line" placeholder="IGST" value="">

                                </div>

                                </div>


                                <div class="col-md-4">
                                <label><strong>SGST</strong></label>
                                <div >
                                    <input type="text" name="sgst" id="sgst" class="form-control form-control-line" placeholder="IGST" value="">

                                </div>

                                </div>
                                <!!-- gst details-->

                                <label><strong>Rate / Product</strong></label>
                                <div >
                                    <input type="text" name="rate_per_product" id="7" class="form-control form-control-line" placeholder=" Average Rate" value="">

                                </div>


                                <label><strong>Amount</strong></label>
                                <div >
                                    <input type="text" name="total_amount" id="6" class="form-control form-control-line" placeholder=" Average Value" value="">

                                </div>




                            </div>
                        </div>




                        <div class="col-md-12">
                            <button type="submit"
                                    class="btn btn-block btn-info btn-lg">
                                Add Stock
                            </button>

                        </div>
                    </form>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; tecions 2017 </footer>
        </div>

    </div>

@section('footerscripts')
    <script src="{{asset('js/jquery.easy-autocomplete.js')}}"></script>

    <script>
        var options = {

            url: "supplier-json",

            getValue: function(element) {
                return element.name;
            },

            list: {
                onSelectItemEvent: function() {
                    var selectedItemValue = $("#supplier_name").getSelectedItemData().id;
                    var selectedItemValue2 = $("#supplier_name").getSelectedItemData().address;
                    var selectedItemValue3 = $("#supplier_name").getSelectedItemData().state;
                    var selectedItemValue4 = $("#supplier_name").getSelectedItemData().gstin;

                    $("#supplier_id").val(selectedItemValue).trigger("change");
                    $("#address").val(selectedItemValue2).trigger("change");
                    $("#gstin").val(selectedItemValue4).trigger("change");
                    $("#state").val(selectedItemValue3).trigger("change");
                },

            }
        };

        $("#supplier_name").easyAutocomplete(options);
    </script>



    <script>
        var options = {

            url: "product-json",

            getValue: function(element) {
                return element.name;
            },

            list: {
                maxNumberOfElements: 10,
                onSelectItemEvent: function() {
                  //  var selectedItemValue = $("#product_name").getSelectedItemData().unit_definition;
                    var selectedItemValue2 = $("#product_name").getSelectedItemData().id;
                    var selectedItemValue3 = $("#product_name").getSelectedItemData().igst;
                    var selectedItemValue4 = $("#product_name").getSelectedItemData().cgst;
                    var selectedItemValue5 = $("#product_name").getSelectedItemData().sgst;
                    var selectedItemValue6 = $("#product_name").getSelectedItemData().unit_definition;

                    $("#product_id").val(selectedItemValue2).unit_definition;
                    $("#igst").val(selectedItemValue3).unit_definition;
                    $("#sgst").val(selectedItemValue5).unit_definition;
                    $("#cgst").val(selectedItemValue4).unit_definition;
                    $("#unit").val(selectedItemValue6).unit_definition;

                }
            }
        };

        $("#product_name").easyAutocomplete(options);
    </script>


@stop
@endsection