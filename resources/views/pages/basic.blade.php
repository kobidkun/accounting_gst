@extends('structure')
@section('body_class', 'fix-header')
@section('content')


  {{--  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   --}}

    <div id="wrapper">
    @include('components.header');
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    @include('components.menu')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Add New Products</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                     <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Get Support</a>
                    <ol class="breadcrumb">
                        <li><a href="/">Dashboard</a></li>
                        <li><a href="/">Manage Products</a></li>
                        <li class="active">Add New Products</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- main page content -->


            <div class="row">

                        <p class="text-muted m-b-30"> </p>
                        <form class="form-material form-horizontal" role="form"  role="form" method="POST" action="{{ route('product.submit') }}">
                            {{csrf_field()}}

                            <div class="col-sm-4">

                                <div class="white-box">
                                    <h3 class="box-title m-b-0"><strong>Add New Products</strong></h3>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                    </div>
                            <div class="form-group">
                                <label><strong>Item Name</strong></label>

                                    <input type="text" name="name" required class="form-control form-control-line" placeholder=" eg: Bajaj Almond Oil 200Ml" value="">
                                    <span class="help-block">
                                        <small>Write the Name of the Product / Item you want to add note: Only Add New Products</small>
                                    </span>

                            </div>


                            <div class="form-group">
                                <label><strong>Item Description</strong></label>

                                    <input type="text" name="description"  class="form-control form-control-line" placeholder=" eg: 200ML Oil" value="">
                                    <span class="help-block">
                                        <small>Write the  Product description</small>
                                    </span>

                            </div>


                                    <div class="form-group">
                                <label><strong>HSN Code</strong></label>

                                        <div class="form-group">


                                                <input type="text" name="hsn" class="form-control form-control-line" placeholder="  9122354" value="">


                                        </div>


                            </div>


                                </div>

                            </div>

                            <div class="col-sm-2">
                            <!--next 0.5-->

                                <div class="white-box">

                                    <h3 class="box-title m-b-0"><strong>Item Group</strong></h3>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                    </div>
                                    <h5 class="m-b-0">Group</h5>
                                    <select name="catogory_id" class="form-control select2">
                                        <option>Select</option>
                                        <optgroup label="Electronics">
                                            <option value="AK">Mobiles</option>
                                            <option value="HI">Television</option>
                                        </optgroup>
                                        <optgroup label="Fruits">
                                            <option value="CA">Banana</option>
                                            <option value="NV">Orange</option>
                                            <option value="OR">Pineapple</option>
                                            <option value="WA">coconut</option>
                                        </optgroup>


                                    </select>


                                    <h5 class="m-t-30">Units </h5>
                                    <select name="unit_definition" class="form-control select2">
                                        <option>Select</option>
                                        <optgroup label="Length">
                                            <option value="cm">cm</option>
                                            <option value="mm">mm</option>
                                            <option value="m">m</option>
                                            <option value="Inch">Inch</option>
                                            <option value="Feet">Feet</option>
                                        </optgroup>
                                        <optgroup label="Weight">
                                            <option value="KG">KG</option>
                                            <option value="Gm">Gm</option>
                                            <option value="Mg">Mg</option>
                                            <option value="Pound">Pound</option>
                                        </optgroup>
                                        <optgroup label="Number">
                                            <option value="Pcs">Pcs</option>
                                            <option value="Dozen">Dozen</option>
                                        </optgroup>


                                    </select>


                                        <h5 class="m-t-30">GST Applicable</h5>


                                            <select name="gst_apply" id="state" class="form-control select2">
                                                <option value="other">Yes</option>
                                                <option value="something">No</option>
                                            </select>




                                        <label>Type of Supply</label>

                                            <select name="service_type" class="form-control">
                                                <option>Goods</option>
                                                <option>Services</option>
                                                <option>Goods & Services</option>
                                            </select>


                                    <br><br><br><br>
                                </div>



                            <!-- next 0.5-->


                            <!-- end full-->
                            </div>



                            <div class="col-sm-3">
                            <!--next 0.5-->

                                <div class="white-box">
                                    <h3 class="box-title m-b-0"><strong>Rate %</strong></h3>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                    </div>
                                    <h5>IGST %</h5>

                                    <div class="form-group">
                                        <div>
                                            <select name="igst" id="province" class="form-control">
                                                <option>0%</option>
                                                <option>5%</option>
                                                <option>12%</option>
                                                <option>18%</option>
                                                <option>28%</option>
                                                <option value="0">Exempted </option>
                                                <option value="0">Non Gst  </option>
                                            </select>
                                        </div>
                                    </div>


                                    <h5 >CGST %</h5>

                                    <div class="form-group">
                                        <div>
                                            <select name="cgst"  id="province2" class="form-control">
                                                <option>0%</option>
                                                <option>2.5</option>
                                                <option>6%</option>
                                                <option>9%</option>
                                                <option>14%</option>
                                                <option value="0">Exempted </option>
                                                <option value="0">Non Gst  </option>
                                            </select>
                                        </div>
                                    </div>







                                    <h5 >SGST %</h5>

                                    <div class="form-group">
                                        <div>
                                            <select name="sgst" id="province3" class="form-control">
                                                <option>0%</option>
                                                <option>2.5</option>
                                                <option>6%</option>
                                                <option>9%</option>
                                                <option>14%</option>
                                                <option value="0">Exempted </option>
                                                <option value="0">Non Gst  </option>
                                            </select>
                                        </div>
                                    </div>


                                    <h5 >Conversion Rate</h5>
                                        <div >
                                            <input type="text" name="conversion_rate" class="form-control form-control-line" placeholder=" eg: 1 PC = 1 Unit" value="">

                                        </div>

                                    <br><br><br>

                                </div>

                            </div>

                           {{--next--}}

                            <div class="col-md-3">
                                <div class="white-box">
                                    <h3 class="box-title m-b-0"><strong>Opening Balance</strong></h3>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                    </div>


                                    <label><strong>Quantity</strong></label>
                                    <div >
                                        <input type="text" name="opeaning_quantity" class="form-control form-control-line" placeholder="Quanty in Stock" value="">

                                    </div>
                                    <label><strong>Rate</strong></label>
                                    <div >
                                        <input type="text" name="opeaning_rate" class="form-control form-control-line" placeholder=" Average Rate" value="">

                                    </div>


                                    <label><strong>Value</strong></label>
                                    <div >
                                        <input type="text" name="opeaning_value" class="form-control form-control-line" placeholder=" Average Value" value="">

                                    </div>
                                    <br><br><br>
                                    <br><br><br>
                                    <br><br><br><br>


                            </div>
                            </div>




                            <div class="col-md-12">
                            <button type="submit"
                                    class="btn btn-block btn-info btn-lg">
                                Save Product
                            </button>

                            </div>
                        </form>
            </div>
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; tecions 2017 </footer>
    </div>

    </div>

@section('footerscripts')


    <!-- end - This is for export functionality only -->
    <script>


        var $state = $('#state'),
            $province = $('#province'),
            $province2 = $('#province2'),
            $province3 = $('#province3')

        ;
        $state.change(function() {
            if ($state.val() == 'other') {
                $province.removeAttr('disabled').val('0%')
                $province2.removeAttr('disabled').val('0%')
                $province3.removeAttr('disabled').val('0%')

                ;
            } else {
                $province.attr('disabled', 'disabled').val('null')
                $province2.attr('disabled', 'disabled').val('null')
                $province3.attr('disabled', 'disabled').val('null')


                ;
            }
        }).trigger('change');
    </script>
@stop
    @endsection