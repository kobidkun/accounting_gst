@extends('structure')
@section('body_class', 'fix-header')
@section('content')


    {{--  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     --}}
    <link href="{{asset('css/easy-autocomplete.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/easy-autocomplete.themes.min.css')}}" rel="stylesheet">
    <div id="wrapper">
        @include('components.header');
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('components.menu')

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Add New Supplier</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Get Support</a>
                        <ol class="breadcrumb">
                            <li><a href="/">Dashboard</a></li>
                            <li><a href="/">Manage Products</a></li>
                            <li class="active">Add New Products</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- main page content -->


                <div class="row">

                    <p class="text-muted m-b-30"> </p>
                    <form class="form-material form-horizontal" role="form"  role="form" method="POST" action="{{ route('supplier.submit') }}">
                        {{csrf_field()}}

                        <div class="col-sm-12">

                            <div class="white-box">
                                <h3 class="box-title m-b-0"><strong>Add New Supplier</strong></h3>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">50% Complete</span></div>
                                </div>
                                <div class="form-group">
                                    <label><strong>Name</strong></label>

                                    <input type="text" name="name" required class="form-control form-control-line" placeholder="Amit Kumar Prasad" value="">
                                    <span class="help-block">
                                        <small>Add Name of Supplier</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>Supplier Address</strong></label>

                                    <input type="text" name="address"  class="form-control form-control-line" placeholder=" Siliguri West Bengal, India" value="">
                                    <span class="help-block">
                                        <small>Add Supplier Address</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>Supplier Mobile Number</strong></label>

                                    <input type="number" name="mobile"  class="form-control form-control-line" placeholder=" +919876543210" value="">
                                    <span class="help-block">
                                        <small>Add Supplier Mobile Number</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>GSTIN</strong></label>

                                    <input type="text" name="gstin" id="gstin"   class="gstin form-control form-control-line" placeholder="22-AAAAA0000A-1ZH">
                                    <span class="help-block">
                                        <small>GSTIN of Supplier</small>
                                    </span>

                                </div>


                                <div class="form-group">
                                    <label><strong>GST Applicable State</strong></label>

                                    <input type="text" name="gstinstate"  class="form-control form-control-line" placeholder="State of Origin" value="">
                                    <span class="help-block">
                                        <small>GSTIN Belonging State</small>
                                    </span>

                                </div>





                            </div>

                        </div>






                        <div class="col-md-12">
                            <button type="submit"
                                    class="btn btn-block btn-info btn-lg">
                                Add Stock
                            </button>

                        </div>
                    </form>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; tecions 2017 </footer>
        </div>

    </div>

@section('footerscripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function(){

            $('.gstin').mask('AA-SSSSSYYYYZ-BCD', {'translation': {
                A: {pattern: /[0-9]/},
                S: {pattern: /[A-Za-z]/},
                Y: {pattern: /[0-9]/},
                Z: {pattern: /[A-Za-z]/},
                B: {pattern: /[0-9]/},
                C: {pattern: /[A-Za-z]/},
                D: {pattern: /[0-9]/}
            }
            });

                    });
    </script>
   <script>
       $(document).ready(function() {
           $("#gstin").keyup(function() {
               var valtext = $(this).val()
               $(this).val(valtext.toUpperCase())
           })
       })
   </script>



@stop
@endsection