<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockRecord;
class ManageStock extends Controller
{
    public function viewStocks(){
        return view('');

    }

    public function AddStock() {
        return view ('pages.addstock');
    }

    public function StoreStock(Request $request) {

        $stock_add = new StockRecord();
        $stock_add->purchase_number =  $request->purchase_number ;
        $stock_add->invoice_number =  $request->invoice_number ;
        $stock_add->date_of_purchase =  $request->date_of_purchase ;
        $stock_add->supplier_name =  $request->supplier_name ;
        $stock_add->supplier_id =  $request->supplier_id ;
        $stock_add->address =  $request->address ;
        $stock_add->state =  $request->state ;
        $stock_add->gstin =  $request->gstin ;
        $stock_add->product_name =  $request->product_name ;
        $stock_add->product_id =  $request->product_id ;
        $stock_add->unit =  $request->unit ;
        $stock_add->igst =  $request->igst ;
        $stock_add->cgst =  $request->cgst ;
        $stock_add->sgst =  $request->sgst ;
        $stock_add->quantity =  $request->quantity ;
        $stock_add->rate_per_product =  $request->rate_per_product ;
        $stock_add->total_amount =  $request->total_amount ;
        $stock_add->save();
        return redirect('list-products');
    }

    public function ViewAllStocks() {

        $stockrecord = StockRecord::all();

        return view('pages.liststock', ['StockRecord' => $stockrecord]);
    }
}
