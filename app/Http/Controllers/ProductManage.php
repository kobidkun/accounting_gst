<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Supplier_details;
class ProductManage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $Products = Product::all();
     return  view('pages.listproducts', ['Products' => $Products]);

      //  $Users = \App\User::all();

   //     return view('admin', ['Users' => $Users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('pages.basic');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $product = new Product();
       $product ->name  = $request->name;
       $product ->catogory_id  = $request->catogory_id;
       $product ->unit_definition  = $request->unit_definition;
       $product ->description  = $request->description;
       $product ->gst_apply  = $request->gst_apply;
       $product ->service_type  = $request->service_type;
       $product ->igst  = $request->igst;
       $product ->cgst  = $request->cgst;
       $product ->sgst  = $request->sgst;
       $product ->hsn  = $request->hsn;
       $product ->conversion_rate  = $request->conversion_rate;
       $product ->opeaning_quantity  = $request->opeaning_quantity;
       $product ->opeaning_rate  = $request->opeaning_rate;
       $product ->conversion_rate  = $request->conversion_rate;
       $product ->opeaning_value  = $request->opeaning_value;
       $product ->save();

       return redirect('list-products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ProductsJson()
    {

        $Products = Product::all();

        echo json_encode($Products);
        //return  view('pages.listproducts', ['Products' => $Products]);

        //  $Users = \App\User::all();

        //     return view('admin', ['Users' => $Users]);
    }

    public function SupplierJson()
    {

        $Products = Supplier_details::all();

        echo json_encode($Products);
        //return  view('pages.listproducts', ['Products' => $Products]);

        //  $Users = \App\User::all();

        //     return view('admin', ['Users' => $Users]);
    }
}
