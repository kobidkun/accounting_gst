<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier_details;

class ManageSupplier extends Controller
{
    public function index() {
        return view('pages.addsupplier');
    }

    public function StoreSupplier(Request $request) {
        $store_supplier = new Supplier_details();
        $store_supplier->name = $request->name ;
        $store_supplier->address = $request->address ;
        $store_supplier->gstinstate = $request->gstinstate ;
        $store_supplier->gstin = $request->gstin ;
        $store_supplier->mobile = $request->mobile ;
        $store_supplier->save() ;
        return redirect('add-supplier');

    }

    public function ViewAllSupplier() {

        $supplier = Supplier_details::all();

       return view('pages.listsupplier', ['Supplier_detailss' => $supplier]);
    }
}
