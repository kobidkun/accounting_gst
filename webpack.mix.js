let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/

mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/sidebar-nav.min.css',
    'public/css/style.css',
    'public/css/animate.css',
    'public/css/megna-dark.css',
], 'public/css/compiled.css')
    .sourceMaps();

mix.scripts([
    'public/js/jquery.min.js',
    'public/js/bootstrap.min.js',
    'public/js/sidebar-nav.min.js',
    'public/js/jquery.slimscroll.js',
    'public/js/waves.js',
    'public/js/custom.min.js',
    'public/js/jQuery.style.switcher.js'
], 'public/js/compiled.js')
    .sourceMaps();
