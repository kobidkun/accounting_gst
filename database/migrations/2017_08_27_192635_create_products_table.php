<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('catogory_id')->nullable();
            $table->text('unit_definition')->nullable();
            $table->text('description')->nullable();
            $table->text('gst_apply')->nullable();
            $table->text('service_type')->nullable();
            $table->text('igst')->nullable();
            $table->text('cgst')->nullable();
            $table->text('sgst')->nullable();
            $table->text('hsn')->nullable();
            $table->text('conversion_rate')->nullable();
            $table->integer('opeaning_quantity')->nullable();
            $table->integer('opeaning_rate')->nullable();
            $table->text('opeaning_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
