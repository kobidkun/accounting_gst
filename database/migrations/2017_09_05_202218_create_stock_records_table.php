<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_records', function (Blueprint $table) {
            $table->increments('id');
            $table->text('purchase_number')->nullable();
            $table->text('invoice_number')->nullable();
            $table->text('date_of_purchase')->nullable();
            $table->text('supplier_name')->nullable();
            $table->text('supplier_id')->nullable();
            $table->text('address')->nullable();
            $table->text('state')->nullable();
            $table->text('gstin')->nullable();
            $table->text('product_name')->nullable();
            $table->text('product_id')->nullable();
            $table->text('unit')->nullable();
            $table->text('igst')->nullable();
            $table->text('cgst')->nullable();
            $table->text('sgst')->nullable();
            $table->text('quantity')->nullable();
            $table->text('rate_per_product')->nullable();
            $table->text('total_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_records');
    }
}
